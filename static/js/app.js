// from data.js
var tbody = d3.select("tbody");

var tableData = data;

// YOUR CODE HERE!
var submit = d3.select("#filter-btn");

submit.on("click", function() {
    d3.event.preventDefault();
    
    var inputElement = d3.select("#datetime");
    var inputValue = inputElement.property("value");
    console.log(inputValue);
    var searchType = selDataset.value;
    console.log(searchType);
    switch (searchType) {
        case "datetime":
            var filteredData = tableData.filter(sighting => sighting.datetime === inputValue);
            break;
        case "city":
            var filteredData = tableData.filter(sighting => sighting.city === inputValue);
            break;
        case "state":
            var filteredData = tableData.filter(sighting => sighting.state === inputValue);
            break;
        }
    // var filteredData = tableData.filter(sighting => sighting.datetime === inputValue);
    console.log(filteredData);
    // console.log(selDataset.value);
    filteredData.forEach((filteredData) => {
        var row = tbody.append("tr");
        Object.entries(filteredData).forEach(([key, value]) => {
          var cell = tbody.append("td");
          // console.log(cell)
          cell.text(value);
        });
    });
});

// clear table

var clear = d3.select("#clear-btn");
clear.on("click", function() {
    d3.select("tr").remove();

});
